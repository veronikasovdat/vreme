# VREME #

=======================

Analizirala bom vreme v različnih krajih po svetu za zadnjih 30 let.
(https://darksky.net/dev)

Za vsak kraj bom glede na casovno obdobje poiskala naslednje podatke:

* temperaturo
* kolicino padavin
* stevilo soncnih, dezevnih dni
* vlago
* hitrost vetra

Hipoteze:

* Povprecna temperatura vecine krajev, ki jih bom preucevala, se je dvignila
* poleg temperature so se spremenili tudi drugi parametri vremena: kolicina padavin, vlaga, hitrost vetra
* nekateri deli sveta so bolj obcutljivi na globalno segrevanje kot drugi